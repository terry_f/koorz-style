/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.constant;
/**
 * 功能描述：miri全局常量
 * 作        者：尹东东 
 * 创建时间：2013-1-6 下午1:58:48
 * 版  本  号：1.0
 */
public class GlobalConstant {
	public static String DOMAIN_HOST = "http://www.koorz.com";
	public static String RESOURCES_HOST = "http://static.koorz.com";
	/**
	 * 记录登录前的操作action
	 */
	public static final String LOGIN_TO_ACTION = "from_to_action";
	public static String ADMIN_TOKEN = "koorz_admin_token";
	public static String USER_TOKEN = "koorz_user_token";
	public static String VISIT_TOKEN = "visit_user_session";
	
	/**
	 * cookie最大期限1周
	 */
	public static int COOKIE_MAX_AGE = 60*60*24*31;
	
	/**
	 * 默认小头像
	 */
	public static String DEFAULT_AVATAR = RESOURCES_HOST+"/ui/img/commons/avatar.png";
	/**
	 * 默认大头像
	 */
	public static String DEFAULT_AVATAR_LARGE = RESOURCES_HOST+"/ui/img/commons/avatar-large.png";
}
