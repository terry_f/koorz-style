/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.web;

import org.apache.commons.lang.StringUtils;

import com.jfinal.aop.Before;
import com.koorz.interceptor.CommonsInterceptor;
import com.koorz.jfinal.route.ControllerBind;
import com.koorz.modules.image.Image;
import com.koorz.modules.style.Style;

/**
 * 功能描述：首页 作 者：尹东东 创建时间：2013-1-6 下午4:37:41 版 本 号：1.0
 */
@ControllerBind(controllerKey = "/", viewPath = "")
public class HomeController extends BaseController {
	
	/**
	 * 
	 * 功能描述：网站首页 
	 * 作 者：尹东东 
	 * 创建时间：2013-1-11 下午8:11:53 
	 * 参 数： 
	 * 返 回:无 
	 * 异 常：无 
	 * 版 本 号：1.0
	 */
	@Before(CommonsInterceptor.class)
	public void index() {
		setAttr("api", "/api/style");
		setAttr("pname", "type");
		setAttr("pvalue", Style.StyleType.STAR.ordinal());
		render("index.html");
	}
	
	/**
	 * 
	 * 功能描述：去购买
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 下午10:21:22
	 * 参       数:单品id
	 * U R L:/goItem/0
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void goItem(){
		String id = getPara();
		if(StringUtils.isEmpty(id)){
			redirect("/",false);
		}else {
			Image image = Image.getImage("id", id);
			redirect(image.getStr("goods_link"), false);
		}
	}
}
