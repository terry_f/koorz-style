package com.koorz.oauth.builder.api;

import com.koorz.oauth.extractors.AccessTokenExtractor;
import com.koorz.oauth.extractors.WeiboTokenExtractor;
import com.koorz.oauth.model.OAuthConfig;
import com.koorz.oauth.model.Verb;
import com.koorz.oauth.utils.OAuthEncoder;
import com.koorz.utils.LoginUtil;

/**
 * SinaWeibo OAuth 2.0 api.
 */
public class SinaWeiboApi extends DefaultApi20 {
	private static final String AUTHORIZE_URL = "https://api.weibo.com/oauth2/authorize?client_id=%s&redirect_uri=%s&state=%s&response_type=code";
	private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL
			+ "&scope=%s";

	@Override
	public Verb getAccessTokenVerb() {
		return Verb.POST;
	}

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new WeiboTokenExtractor();
	}

	@Override
	public String getAccessTokenEndpoint() {
		return "https://api.weibo.com/oauth2/access_token?grant_type=authorization_code";
	}

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		// Append scope if present
		if (config.hasScope()) {
			return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					OAuthEncoder.encode(LoginUtil.createState(config.getState())),
					OAuthEncoder.encode(config.getScope()));
		} else {
			return String.format(AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					LoginUtil.createState(config.getState()));
		}
	}
}
