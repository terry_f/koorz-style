/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.builder.api;

import com.koorz.oauth.extractors.AccessTokenExtractor;
import com.koorz.oauth.extractors.DoubanTokenExtractor;
import com.koorz.oauth.model.OAuthConfig;
import com.koorz.oauth.model.Verb;
import com.koorz.oauth.utils.OAuthEncoder;
import com.koorz.utils.LoginUtil;

/**
 * 功能描述： 作 者：尹东东 创建时间：2013-3-27 下午9:08:21 版 本 号：1.0
 */
public class DoubanApi extends DefaultApi20 {
	private static final String AUTHORIZE_URL = "https://www.douban.com/service/auth2/auth?client_id=%s&redirect_uri=%s&state=%s&response_type=code";
	private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL
			+ "&scope=%s";

	@Override
	public Verb getAccessTokenVerb() {
		return Verb.POST;
	}

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new DoubanTokenExtractor();
	}

	@Override
	public String getAccessTokenEndpoint() {
		return "https://www.douban.com/service/auth2/token?grant_type=authorization_code";
	}

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		// Append scope if present
		if (config.hasScope()) {
			return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					OAuthEncoder.encode(LoginUtil.createState(config.getState())),
					OAuthEncoder.encode(config.getScope()));
		} else {
			return String.format(AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					OAuthEncoder.encode(LoginUtil.createState(config.getState())));
		}
	}

}
