/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model.weibo;

import com.koorz.oauth.model.Token;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午4:01:52
 * 版  本  号：1.0
 */
public class WeiboToken extends Token<WeiboToken>{
	private static final long serialVersionUID = -5594381327877835778L;
	private String uid = null;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public WeiboToken() {
		super();
	}
	public WeiboToken(String accessToken, int expiresIn, String refreshToken,String uid) {
		super(accessToken, expiresIn, refreshToken);
		this.uid = uid;
	}
}
