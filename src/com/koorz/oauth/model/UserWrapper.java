/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model;

import java.io.Serializable;

/**
 * 功能描述：第三方平台用户信息
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午10:30:07
 * 版  本  号：1.0
 */
public class UserWrapper implements Serializable{
	private static final long serialVersionUID = 4142077895747710877L;
	//用户UID
	private String uid;
	//用户昵称
	private String nickname;
	//用户所在地
	private String loc;
	//用户个人描述
	private String desc;
	//性别，1：男、0：女、2：未知
	private int gender;
	//用户大头像地址100×100
	private String avatarLarge;
	//用户头像地址，50×50像素
	private String avatar;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getAvatarLarge() {
		return avatarLarge;
	}
	public void setAvatarLarge(String avatarLarge) {
		this.avatarLarge = avatarLarge;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	@Override
	public String toString() {
		return nickname;
	}
}
