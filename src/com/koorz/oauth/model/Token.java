package com.koorz.oauth.model;

import java.io.Serializable;
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class Token<M extends Token> implements Serializable {
	private static final long serialVersionUID = 715000866082812683L;
	private String accessToken = null;
	private Integer expiresIn = null;
	private String refreshToken = null;
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Integer getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public Token() {}
	  
	public Token (String accessToken) {
		this.accessToken = accessToken;
	}
  
	public Token (String accessToken, int expiresIn, String refreshToken) {
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.refreshToken = refreshToken;
	}
}
