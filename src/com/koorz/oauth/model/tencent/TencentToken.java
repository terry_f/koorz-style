/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model.tencent;

import com.koorz.oauth.model.Token;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午6:31:22
 * 版  本  号：1.0
 */
public class TencentToken extends Token<TencentToken>{
	private static final long serialVersionUID = -6148170066106420808L;
}
