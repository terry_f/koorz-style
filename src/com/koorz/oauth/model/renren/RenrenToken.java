/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model.renren;

import com.koorz.oauth.model.Token;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午6:21:31
 * 版  本  号：1.0
 */
public class RenrenToken extends Token<RenrenToken>{
	private static final long serialVersionUID = 6798675178905127929L;
	private String scope;
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public RenrenToken() {
		super();
	}
	public RenrenToken(String accessToken, int expiresIn, String refreshToken,String scope) {
		super(accessToken, expiresIn, refreshToken);
		this.scope = scope;
	}
}
