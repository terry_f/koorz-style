package com.koorz.oauth.services;

import java.util.List;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.koorz.modules.user.User;
import com.koorz.oauth.model.OAuthRequest;
import com.koorz.oauth.model.Token;
import com.koorz.oauth.model.UserWrapper;
import com.koorz.oauth.model.Verb;

public abstract class ApiService {

	protected OAuthRequest createRequest(Verb verb, String url) {
		return new OAuthRequest(verb, url);
	}
	
	/**
	 * 
	 * 功能描述：绑定用户
	 * 作        者：尹东东
	 * 创建时间：2013-4-13 下午12:26:40
	 * 参       数：
	 * 返       回:返回用户
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@SuppressWarnings("rawtypes")
	public abstract User bindUser(Token objClass);
	
	/**
	 * 
	 * 功能描述：封装用户信息
	 * 作        者：尹东东
	 * 创建时间：2013-4-9 下午1:40:15
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected abstract UserWrapper wapperUser(JSONObject obj);
	
	/**
	 * 
	 * 功能描述：封装用户列表信息
	 * 作        者：尹东东
	 * 创建时间：2013-4-9 下午1:40:27
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected abstract List<UserWrapper> wapperUsers(JSONArray array);
}
