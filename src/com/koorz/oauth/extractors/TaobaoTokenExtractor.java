/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.extractors;

import com.alibaba.fastjson.JSONObject;
import com.koorz.oauth.model.Token;
import com.koorz.oauth.model.taobao.TaobaoToken;
import com.koorz.oauth.utils.Preconditions;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-31 下午11:03:45
 * 版  本  号：1.0
 */
public class TaobaoTokenExtractor implements AccessTokenExtractor{

	@Override
	public Token<?> extract(String response) {
		Preconditions.checkEmptyString(response,
				"Cannot extract a token from a null or empty String");
		JSONObject json = JSONObject.parseObject(response);
		TaobaoToken token = new TaobaoToken();
		if (json.containsKey("access_token")) {
		      token.setAccessToken(json.getString("access_token"));
		}
		if(json.containsKey("refresh_token")){
			token.setRefreshToken(json.getString("refresh_token"));
		}
		if (json.containsKey("expires_in")) {
		      token.setExpiresIn(json.getIntValue("expires_in"));
		}
		if (json.containsKey("taobao_user_nick")) {
			token.setNick(json.getString("taobao_user_nick"));
		}
		if (json.containsKey("taobao_user_id")) {
			token.setUid(json.getString("taobao_user_id"));
		}
		return token;
	}
}
