/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.extractors;


import org.apache.commons.lang.StringUtils;

import com.koorz.oauth.model.Token;
import com.koorz.oauth.model.tencent.TencentToken;
import com.koorz.oauth.utils.Preconditions;


/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午6:35:02
 * 版  本  号：1.0
 */
public class TencentTokenExtractor implements AccessTokenExtractor{

	@Override
	public Token<?> extract(String response) {
		Preconditions.checkEmptyString(response,
				"Cannot extract a token from a null or empty String");
		String[] tmps = StringUtils.split(response, "&");
		TencentToken token = new TencentToken();
		for(String str:tmps){
			if(StringUtils.startsWith(str, "access_token")){
				token.setAccessToken(StringUtils.substringAfterLast(str, "="));
			}else if(StringUtils.startsWith(str, "expires_in")){
				token.setExpiresIn(Integer.parseInt(StringUtils.substringAfterLast(str, "=")));
			}
		}
		return token;
	}

}
