/**
 * 
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import java.util.List;

/**
 * 功能描述：定义用于获取索引更新任务的接口
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午9:32:47
 * 版  本  号：1.0
 */
public interface IndexTasker {

	/**
	 * 获取所有待处理的索引任务
	 * @return
	 */
	public List<IndexTask> list();
	
}
