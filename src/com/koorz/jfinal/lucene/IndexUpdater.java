/**
 * 
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import java.util.Arrays;
import java.util.List;

import com.jfinal.log.Logger;

/**
 * 索引增量更新
 * 该类接受两个参数
 * idx_path 索引根目录
 * tasker 任务获取实现类
 * 
 * 对应 build.xml 的配置
 * 
    <target depends="lucene_init" name="lucene_build">
        <echo message="Build lucene index of ${ant.project.name}"/>    	
		<java classname="net.oschina.search.IndexUpdater" classpathref="oschina.classpath" fork="true">
			<jvmarg value="-Xmx512m" />			
			<arg value="-p" />			
			<arg value="${lucene.dir}" />			
			<arg value="-t" />			
			<arg value="net.oschina.search.IndexTasker" />
		</java>
	</target>
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午9:36:05
 * 版  本  号：1.0
 */
public class IndexUpdater {
	
	private final static Logger log = Logger.getLogger(IndexUpdater.class);
	
	public static void main(String[] args) throws Exception {
		//参数解析
		
		IndexTasker tasker = null;
		//初始化索引管理器
		IndexHolder holder = IndexHolder.bulider();
		
		//处理索引更新
		List<IndexTask> tasks = tasker.list();

		for(IndexTask task : tasks) {
			execute(holder, task, true);
		}

		if(tasks.size()>0)
			log.info(tasks.size()+ " Index tasks executed finished.");
	}
	
	/**
	 * 处理单个索引任务
	 * @param holder
	 * @param task
	 * @param update_status
	 * @throws Exception
	 */
	private static void execute(IndexHolder holder, IndexTask task, boolean update_status) throws Exception {
		Searchable obj = (Searchable)task.object();
		if(obj != null){
			switch(task.getOpt()){
			case IndexTask.OPT_ADD:
				holder.add(Arrays.asList(obj));
				break;
			case IndexTask.OPT_DELETE:
				holder.delete(Arrays.asList(obj));
				break;
			case IndexTask.OPT_UPDATE:
				holder.update(Arrays.asList(obj));
			}
			if(update_status)
				task.afterBuild();
		}
	}

}
