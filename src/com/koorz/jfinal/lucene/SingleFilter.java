/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import java.io.IOException;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

/**
 * 功能描述：一个处理字词混合的Filter
 * 作        者：尹东东 
 * 创建时间：2013-4-3 下午9:39:48
 * 版  本  号：1.0
 */
public class SingleFilter extends TokenFilter {

	private CharTermAttribute  termAtt;
	private OffsetAttribute offsetAtt;
	private PositionIncrementAttribute posIncr;
	private char[] curTermBuffer;
	private int curPos;
	private int curTermLength;
	private int i = 0;

	public SingleFilter(TokenStream in) {
		super(in);
		this.termAtt = addAttribute(CharTermAttribute.class);
		this.offsetAtt = addAttribute(OffsetAttribute.class);
		this.posIncr = addAttribute(PositionIncrementAttribute.class);
	}

	@Override
	public boolean incrementToken() throws IOException {
		if (curTermBuffer == null) {
			if (!input.incrementToken()) {
				return false;
			} else {
				curTermBuffer = (char[]) termAtt.buffer().clone();
				curTermLength = termAtt.length();
				termAtt.copyBuffer(curTermBuffer, 0, curTermLength);
				curPos = 1;
				i = offsetAtt.startOffset();
				return true;

			}
		}
		// 本循环只是把词分成单个字
		if (curPos <= curTermLength && curTermLength > 1) {
			offsetAtt.setOffset(i, i + 1);
			termAtt.copyBuffer(curTermBuffer, curPos - 1, 1);
			posIncr.setPositionIncrement(0);
			curPos++;
			i++;
			return true;
		}
		curTermBuffer = null;
		return incrementToken();
	}
}
