/**
 * 
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import java.util.List;
import java.util.Map;

/**
 * 功能描述：可搜索对象
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午9:35:31
 * 版  本  号：1.0
 */
public interface Searchable extends Comparable<Searchable> {

	public String FN_TITLE 	= "title";
	public String FN_DETAIL	= "detail";
	
    /**
     * 文档的唯一编号
     * @return 文档id
     */
    public String uniqueId();

    /**
     * 要存储的字段
     * @return 返回字段名列表
     */
    public List<String> storeFields();

    /**
     * 要进行分词索引的字段
     * @return 返回字段名列表
     */
    public List<String> indexFields();

    /**
     * 文档的优先级
     * @return
     */
    public float boost();
    
    /**
     * 扩展的存储数据
     * @return 扩展数据K/V
     */
    public Map<String, String> extendStoreDatas();

    /**
     * 扩展的索引数据
     * @return 扩展数据K/V
     */
    public Map<String, String> extendIndexDatas();

    /**
     * 列出id值大于指定值得所有对象
     * @param id
     * @param count
     * @return
     */
	public List<? extends Searchable> ListAfter(int page,int count) ;
}
