/**
 * 
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

/**
 * 功能描述：索引更新操作
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午9:29:53
 * 版  本  号：1.0
 */
public interface IndexTask {

	public int OPT_ADD 	= 0x01;	//添加索引
	public int OPT_UPDATE 	= 0x02;	//更新索引
	public int OPT_DELETE 	= 0x04;	//删除索引
	
	/**
	 * 返回更新操作类型
	 * @return 请看上面三个常量
	 */
	public int getOpt();
	
	/**
	 * 返回对应的可搜索对象
	 * @return
	 */
	public Searchable object();
	
	/**
	 * 当该索引更新操作完毕后执行此方法
	 */
	public void afterBuild();
	
}
