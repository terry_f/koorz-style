package com.koorz.jfinal.tablebind;

public interface INameStyle {

    String name(String className);

}
