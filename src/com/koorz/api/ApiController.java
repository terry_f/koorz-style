/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.api;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.koorz.jfinal.route.ControllerBind;
import com.koorz.modules.image.Image;
import com.koorz.modules.style.Style;

/**
 * 功能描述：数据接口
 * 作        者：尹东东 
 * 创建时间：2013-5-28 下午2:18:46
 * 版  本  号：1.0
 */
@ControllerBind(controllerKey="/api",viewPath="")
public class ApiController extends Controller{

	/*
	 * 1、用户接口/user
	 * 2、单品接口/single
	 * 3、搭配接口/style
	 */
	
	/************************************************用户接口************************************************************/
	
	/**
	 * 
	 * 功能描述：获取用户发布过的搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午4:54:28
	 * 参       数:用户id
	 * U R L:/api/user/id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void user(){
		String uid = getPara();
		if(!StringUtils.isEmpty(uid))
			renderJson(Style.getUserStyles(getParaToInt("page", 1), getParaToInt("pageSize", 10),uid));
		else 
			renderNull();
	}
	
	/************************************************单品接口************************************************************/
	
	/**
	 * 
	 * 功能描述：获取所有单品下的数据；当有分类时必须是具体的分类值，当标签组为0时表示参数为空，当标签为0时表示参数为空；
	 * /api/single/100000-0-0表示查该分类下所有数据；
	 * /api/single/0-102020-0表示查该分类标签组下所有数据；
	 * /api/single/100000-0-102020表示查该分类标签下所有数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午4:57:39
	 * 参       数:
	 * U R L:/api/single/分类-标签组-标签?page=1&pageSize=10
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void single(){
		/**
		 * 1、所有
		 * 2、分类
		 * 3、标签组
		 * 4、标签组-标签
		 * 5、分类-0-标签
		 */
		String cid = getPara(0);
		String lg = getPara(1);
		String l = getPara(2);
		if((StringUtils.isEmpty(cid) || StringUtils.equals(cid, "0"))
				&& (StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))
				&& (StringUtils.isEmpty(l) || StringUtils.equals(l, "0"))){ //所有 0-0-0
			renderJson(Image.getSingles(getParaToInt("page", 1), getParaToInt("pageSize", 10)));
		}else if(StringUtils.isNotEmpty(lg) && !StringUtils.equals(lg, "0")
				&& StringUtils.isNotEmpty(l) && !StringUtils.equals(l, "0")){ //0-标签组-标签
			renderJson(Image.getSinglesByLabelGroupLabel(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg,l));
		}else if(StringUtils.isNotEmpty(lg) && !StringUtils.equals(lg, "0")
				&& (StringUtils.isEmpty(l) || StringUtils.equals(l, "0"))){//0-标签组-0
			renderJson(Image.getSinglesByLabelGroup(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg));
		}else if(StringUtils.isNotEmpty(cid) && !StringUtils.equals(cid, "0")
				&& StringUtils.isNotEmpty(l) && !StringUtils.equals(l, "0")
				&& (StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))){//分类-0-标签
			renderJson(Image.getSinglesByCategoryLabel(getParaToInt("page", 1), getParaToInt("pageSize", 10),cid,l));
		}else if(StringUtils.isNotEmpty(cid) && !StringUtils.equals(cid, "0")
				&& (StringUtils.isEmpty(l) || StringUtils.equals(l, "0"))
				&& (StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))){//分类-0-0
			renderJson(Image.getSinglesByCategory(getParaToInt("page", 1), getParaToInt("pageSize", 10),cid));
		}else if(StringUtils.isNotEmpty(l) && !StringUtils.equals(l, "0")
				&& (StringUtils.isEmpty(cid) || StringUtils.equals(cid, "0"))
				&& (StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))){//0-0-标签
			renderJson(Image.getSinglesByLabel(getParaToInt("page", 1), getParaToInt("pageSize", 10),l));
		}
	}
	
	/************************************************搭配接口************************************************************/
	
	/**
	 * 
	 * 功能描述：获取所有搭配数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午4:57:30
	 * 参       数:
	 * U R L:/api/style/标签组-标签?page=1&pageSize=10&type=1
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void style(){
		/**
		 * 1、所有
		 * 2、标签组
		 * 3、标签组-标签
		 * 4、0-标签
		 */
		String lg = getPara(0);
		String l = getPara(1);
		int type = getParaToInt("type",-1);
		if((StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))
				&& (StringUtils.isEmpty(l) || StringUtils.equals(l, "0"))){ //所有 0-0
			renderJson(Style.getStyles(getParaToInt("page", 1), getParaToInt("pageSize", 10),type));
		}else if(StringUtils.isNotEmpty(lg) && !StringUtils.equals(lg, "0")
				&& StringUtils.isNotEmpty(l) && !StringUtils.equals(l, "0")){ //标签组-标签
			renderJson(Style.getStylesByLabelGroupLabel(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg,l,type));
		}else if(StringUtils.isNotEmpty(lg) && !StringUtils.equals(lg, "0")
				&& (StringUtils.isEmpty(l) || StringUtils.equals(l, "0"))){//标签组-0
			renderJson(Style.getStylesByLabelGroup(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg,type));
		}else if(StringUtils.isNotEmpty(l) && !StringUtils.equals(l, "0")
				&& (StringUtils.isEmpty(lg) || StringUtils.equals(lg, "0"))){//0-标签
			renderJson(Style.getStylesByLabel(getParaToInt("page", 1), getParaToInt("pageSize", 10),l,type));
		}
	}
	
	/************************************************相关推荐接口************************************************************/
	
	/**
	 * 
	 * 功能描述：相关推荐
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午5:03:32
	 * 参       数:
	 * U R L:/api/related
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void related(){
		
	}
	
	/**
	 * 
	 * 功能描述：获取模板 单品模板 、明星/街拍达人模板、主题模板;现在只获取主题模板
	 * 作        者：尹东东
	 * 创建时间：2013-6-5 上午11:18:47
	 * 参       数:
	 * U R L:/api/templet/标签组-标签
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void templet(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			renderJson(Image.getTemplet(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg,l));
		}else if(StringUtils.isNotEmpty(lg)){
			renderJson(Image.getTemplet(getParaToInt("page", 1), getParaToInt("pageSize", 10),lg,null));
		}
	}
	
	/**
	 * 
	 * 功能描述：获取用户订单列表
	 * 作        者：尹东东
	 * 创建时间：2013-6-19 下午4:57:51
	 * 参       数:uid用户id
	 * U R L:/api/orderList
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void orderList(){
		String uid = getPara("uid");
		if(StringUtils.isNotEmpty(uid)){
			String select = "select *";
			String sqlExceptSelect = "from order_form where user_id=? order by pay_time desc";
			renderJson(Db.paginate(getParaToInt("page", 1), getParaToInt("pageSize", 10), select, sqlExceptSelect,uid));
		}else {
			renderJson(new Page<Record>(new ArrayList<Record>(0), getParaToInt("page", 1), getParaToInt("pageSize", 10), 0, 0));
		}
	}
	
	/**
	 * 
	 * 功能描述：获取用户奖励记录
	 * 作        者：尹东东
	 * 创建时间：2013-6-19 下午5:17:06
	 * 参       数:uid用户id
	 * U R L:/api/rewardList
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void rewardList(){
		String uid = getPara("uid");
		if(StringUtils.isNotEmpty(uid)){
			String select = "select *";
			String sqlExceptSelect = "from reward_record where user_id=? order by create_time desc";
			renderJson(Db.paginate(getParaToInt("page", 1), getParaToInt("pageSize", 10), select, sqlExceptSelect,uid));
		}else {
			renderJson(new Page<Record>(new ArrayList<Record>(0), getParaToInt("page", 1), getParaToInt("pageSize", 10), 0, 0));
		}
	}
	
	/**
	 * 
	 * 功能描述：获取用户提现记录
	 * 作        者：尹东东
	 * 创建时间：2013-6-19 下午5:41:41
	 * 参       数:uid用户id
	 * U R L:/api/drawList
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CacheInterceptor.class)
	public void drawList(){
		String uid = getPara("uid");
		if(StringUtils.isNotEmpty(uid)){
			String select = "select *";
			String sqlExceptSelect = "from draw_record where user_id=? order by create_time desc";
			renderJson(Db.paginate(getParaToInt("page", 1), getParaToInt("pageSize", 10), select, sqlExceptSelect,uid));
		}else {
			renderJson(new Page<Record>(new ArrayList<Record>(0), getParaToInt("page", 1), getParaToInt("pageSize", 10), 0, 0));
		}
	}
}
