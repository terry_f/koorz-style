package com.koorz.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * 格式 yyyy-MM-dd
	 */
	public static final SimpleDateFormat format1 = new SimpleDateFormat(
			"yyyy-MM-dd");

	/**
	 * 格式 HH:mm:ss
	 */
	public static final SimpleDateFormat format2 = new SimpleDateFormat(
			"HH:mm:ss");

	/**
	 * 格式 yyyy-MM-dd HH:mm:ss
	 */
	public static final SimpleDateFormat format3 = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	/**
	 * 格式 yyyy/MM/dd
	 */
	public static final SimpleDateFormat format4 = new SimpleDateFormat(
			"yyyy/MM/dd");

	/**
	 * 格式 yyyyMMdd
	 */
	public static final SimpleDateFormat format5 = new SimpleDateFormat(
			"yyyyMMdd");

	/**
	 * 格式 HHmmss
	 */
	public static final SimpleDateFormat format6 = new SimpleDateFormat(
			"HHmmss");

	/**
	 * 格式 yyyyMMddHHmmss
	 */
	public static final SimpleDateFormat format7 = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	/**
	 * 格式 yyyy
	 */
	public static final SimpleDateFormat format8 = new SimpleDateFormat("yyyy");

	/**
	 * 格式 MM
	 */
	public static final SimpleDateFormat format9 = new SimpleDateFormat("MM");

	/**
	 * 格式 dd
	 */
	public static final SimpleDateFormat format10 = new SimpleDateFormat("dd");

	/**
	 * 格式 HH
	 */
	public static final SimpleDateFormat format11 = new SimpleDateFormat("HH");

	/**
	 * 格式 mm
	 */
	public static final SimpleDateFormat format12 = new SimpleDateFormat("mm");

	/**
	 * 格式 ss
	 */
	public static final SimpleDateFormat format13 = new SimpleDateFormat("ss");

	/**
	 * 格式 ss
	 */
	public static final SimpleDateFormat format14 = new SimpleDateFormat(
			"yyyy-MM");

	/**
	 * 根据样式得到格式化对象SimpleDateFormat
	 * 
	 * @param date
	 * @param style
	 * @return
	 */
	public static String getDate(Date date, String style) {
		SimpleDateFormat format = new SimpleDateFormat(style);
		return format.format(date);
	}
	

	/**
	 * 得到某日期的日期部分
	 * 
	 * @param date
	 */
	public static String getAllDate(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return df.format(df.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date getDate(String date) {
		try {
			return format1.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 得到某日期的日期部分
	 * 
	 * @param date
	 * @return yyyy-MM-dd
	 */
	public static String getDate(Date date) {
		return format1.format(date);
	}

	public static String getDateS(String date) {
		return getDate(getDate(date));
	}
	/**
	 * 得到某日期的时间部分
	 * 
	 * @param date
	 * @return HH:mm:ss
	 */
	public static String getTime(Date date) {
		return format2.format(date);
	}
	
	/**
	 * 
	 * 功能描述：当前日期是星期几
	 * 作        者：尹东东
	 * 创建时间：2013-2-27 下午5:00:06
	 * 参       数：date
	 * 返       回:0,1,2,3,4,5,6
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static int getWeekOfDate(Date date) {
//		SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
//		String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
//		return dateFm.format(date);
		
		Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return w;
	}
	
	/**
	 * 得到某日期加上或减去天数后的日期,day为负数时减去
	 * 
	 * @param date
	 * @param month
	 * @return "yyyy-MM-dd"
	 */
	public static String getDayAdd(Date date, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);
		return format1.format(calendar.getTime());
	}

	/**
	 * 得到某日期加上或减去月份后的日期,month为负数时减去
	 * 
	 * @param date
	 * @param month
	 * @return "yyyy-MM-dd"
	 */
	public static String getMonthAdd(Date date, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return format1.format(calendar.getTime());
	}

	/**
	 * 得到某日期加上或减去月份后的日期,month为负数时减去
	 * 
	 * @param date
	 * @param month
	 * @return "yyyy-MM-dd"
	 */
	public static String getHourAdd(Date date, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, hours);
		return format3.format(calendar.getTime());
	}
	
	/**
	 * 
	 * 功能描述：得到几分钟后的时间（前面或后面）
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 下午5:36:22
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String getMinuteAdd(Date date, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minute);
		return format3.format(calendar.getTime());
	}
	
	/**
	 * 得到某日期加上或减去月份后的日期,month为负数时减去
	 * 
	 * @param date
	 * @param month
	 * @return "yyyy-MM-dd HH:Mi:ss"
	 */
	public static String getHourAdd(String dateTime, int hours) {
		Date date = null;;
		try {
			if(dateTime!=null&&dateTime.trim().length()>1){
				date = format3.parse(dateTime);
			}else{
				date = format3.parse("1900-01-01 01:01:01");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, hours);
		return format3.format(calendar.getTime());
	}
	/**
	 * 得到某日期的月份
	 * 
	 * @param Date
	 *            date
	 * @return
	 */
	public static String getMonth(Date date) {
		return format9.format(date);
	}

	/**
	 * 得到某日期的年份
	 * 
	 * @param Date date
	 * @return
	 */
	public static String getYear(Date date) {
		return format8.format(date);
	}

	/**
	 * 得到某年有多少天
	 * 
	 * @param String
	 *            date "yyyy-MM-dd"
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForYear(String date) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(format1.parse(date));
		return calendar.DAY_OF_YEAR;
	}

	/**
	 * 得到某年有多少天
	 * 
	 * @param Date
	 *            date
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForYear(Date date) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.DAY_OF_YEAR;
	}

	/**
	 * 得到某年有多少天
	 * 
	 * @param String
	 *            year "yyyy"
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForYear_YYYY(String year) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(format8.parse(year));
		return calendar.DAY_OF_YEAR;
	}

	/**
	 * 得到某月有多少天
	 * 
	 * @param String
	 *            date "yyyy-MM-dd"
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForMonth(String date) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(format1.parse(date));
		return calendar.DAY_OF_MONTH;
	}

	/**
	 * 得到某月有多少天
	 * 
	 * @param String
	 *            date "yyyy-MM"
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForMonth_MM(String date) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(format14.parse(date));
		return calendar.DAY_OF_MONTH;
	}

	/**
	 * 得到某月有多少天
	 * 
	 * @param Date
	 *            date
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings("static-access")
	public static int getDaysForMonth(Date date) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.DAY_OF_MONTH;
	}

	/**
	 * 得到当前的日期
	 * 
	 * @return
	 */
	public static String getNowDate() {
		return format1.format(new Date());
	}

	/**
	 * 得到当前的时间
	 * 
	 * @return
	 */
	public static String getNowTime() {
		return format2.format(new Date());
	}
	
	/**
	 * 得到当前的时间
	 * yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getNowDateTime() {
		return format3.format(new Date());
	}
	/**
	 * 得到两个时间之间的秒差
	 * @param date1 yyyy-MM-dd HH:mm:ss
	 * @param date2 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static long getSecDiff(String date1,String date2) {
		if(date1==null||date2==null){
			return 0;
		}
		long dayNumber = 0;
		long sec = 1000L;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			java.util.Date d1 = df.parse(date1);
			java.util.Date d2 = df.parse(date2);
			dayNumber = (d2.getTime() - d1.getTime()) / sec;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dayNumber;
	}
	/**
	 * 得到两个时间之前的分差
	 * @param date1 yyyy-MM-dd HH:mm:ss
	 * @param date2 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static long getDiff(String date1,String date2) {
		if(date1==null||date2==null){
			return 0;
		}
		long dayNumber = 0;
		// 1小时=60分钟=3600秒=3600000
		long mins = 60L * 1000L;
		// long day= 24L * 60L * 60L * 1000L;计算天数之差
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			java.util.Date d1 = df.parse(date1);
			java.util.Date d2 = df.parse(date2);
			dayNumber = (d2.getTime() - d1.getTime()) / mins;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dayNumber;
	}
	/**
	 * 得到两个时间之前的差
	 * @param date1 yyyy-MM-dd HH:mm:ss
	 * @param date2 yyyy-MM-dd HH:mm:ss
	 * @return X小时X分钟X秒
	 */
	public static String getDiffInfo(String date1,String date2) {
		StringBuffer result = new StringBuffer();
		if(date1==null||date2==null){
			return "0秒";
		}
		long _day = 0;
		long _day_m_sec = 1000L * 60L * 60L * 24;
		long _hour = 0;
		long _hour_m_sec = 1000L * 60L * 60L;
		long _min = 0;
		long _min_m_sec = 1000L * 60L;
		long _sec = 0;
		long _sec_m_sec = 1000L;
		long dayNumber = 0;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			java.util.Date d1 = df.parse(date1);
			java.util.Date d2 = df.parse(date2);
			dayNumber = (d2.getTime() - d1.getTime());
			_day = dayNumber/_day_m_sec;
			_hour = (dayNumber%_day_m_sec)/_hour_m_sec;
			_min = ((dayNumber%_day_m_sec)%_hour_m_sec)/_min_m_sec;
			_sec = (((dayNumber%_day_m_sec)%_hour_m_sec)%_min_m_sec)/_sec_m_sec;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(_day>0){
			result.append(_day+"天");
		}
		if(_hour>0){
			result.append(_hour+"小时");
		}
		if(_min>0){
			result.append(_min+"分");
		}
		if(_sec>0){
			result.append(_sec+"秒");
		}
		return result.toString();
	}
	/**
	 * 得到两个日期之前的差
	 * @param date1 yyyy-MM-dd
	 * @param date2 yyyy-MM-dd
	 * @return X天
	 */
	public static long getDiffInfoByDay(String date1,String date2) {
		if(date1==null||date2==null){
			return 0;
		}
		long _day = 0;
		long _day_m_sec = 1000L * 60L * 60L * 24;
		long dayNumber = 0;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date d1 = df.parse(date1);
			java.util.Date d2 = df.parse(date2);
			dayNumber = (d2.getTime() - d1.getTime());
			_day = dayNumber/_day_m_sec;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return _day;
	}
	/**
	 * 把毫秒转换成天时分秒
	 * long time 
	 * @return X小时X分钟X秒
	 */
	public static String chageToDHMS(long dayNumber) {
		StringBuffer result = new StringBuffer();
		if(dayNumber==0){
			return "0秒";
		}
		long _day = 0;
		long _day_m_sec = 1000L * 60L * 60L * 24;
		long _hour = 0;
		long _hour_m_sec = 1000L * 60L * 60L;
		long _min = 0;
		long _min_m_sec = 1000L * 60L;
		long _sec = 0;
		long _sec_m_sec = 1000L;
		try {
			_day = dayNumber/_day_m_sec;
			_hour = (dayNumber%_day_m_sec)/_hour_m_sec;
			_min = ((dayNumber%_day_m_sec)%_hour_m_sec)/_min_m_sec;
			_sec = (((dayNumber%_day_m_sec)%_hour_m_sec)%_min_m_sec)/_sec_m_sec;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(_day>0){
			result.append(_day+"天");
		}
		if(_hour>0){
			result.append(_hour+"小时");
		}
		if(_min>0){
			result.append(_min+"分");
		}
		if(_sec>0){
			result.append(_sec+"秒");
		}
		return result.toString();
	}
	
	/**
	 * 
	 * 功能描述：得到几个月后的时间
	 * 创建时间：2013-1-23 上午10:37:14
	 */
	public static String getMonthAfter(String date1,int amount){
		Calendar   Today=   Calendar.getInstance(); 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d1 = df.parse(date1);
			Today.setTime(d1); 
			Today.add(Calendar.MONTH,amount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return df.format(Today.getTime());
	}
	
	/**
	 * 
	 * 功能描述：得到几天后的时间
	 * 创建时间：2013-1-23 上午10:37:47
	 */
	public static String getDayAfter(String date,int amount){
		Calendar today =   Calendar.getInstance(); 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d1 = df.parse(date);
			today.setTime(d1);
			today.add(Calendar.DAY_OF_MONTH,amount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return df.format(today.getTime());
	}
	
	/**
	 * 
	 * 功能描述：得到几天后的日期
	 * 创建时间：2013-1-23 上午10:37:47
	 */
	public static String getDateDayAfter(String date,int amount){
		Calendar today =   Calendar.getInstance(); 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date d1 = df.parse(date);
			today.setTime(d1);
			today.add(Calendar.DAY_OF_MONTH,amount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return df.format(today.getTime());
	}
	
	/**
	 * 
	 * 日期格式转换
	 * 从YYYY-MM-DD转换到YYYYMMDD
	 * @param date
	 * @throws ParseException 
	 */
	public static String changeDateFormat(String dateString){
		Date date;
		String reslut = null;
		try {
			if("".equals(dateString)){
				dateString="0000-00-00";
			}
			date = format1.parse(dateString);
			reslut = format5.format(date);
		} catch (ParseException e) {
			try {
				date = format1.parse("0000-00-00");
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		return reslut;
	}
	
	/**
	 * 日期转换成*分钟内/小时内/10天内，超过10天的按原日期输出
	 * @param date
	 * @param format
	 * @return
	 */
	public static String getCompareTime(Date date,SimpleDateFormat format) {
		String r = "";
		if (date == null)
			return r;
		if (format == null)
			format = format3;

		long nowtimelong = System.currentTimeMillis();
		long ctimelong = date.getTime();
		long result = Math.abs(nowtimelong - ctimelong);

		// 一分钟内
		if (result < 60000) {
			long seconds = result / 1000;
			r = seconds + "秒钟前";
		} else if (result >= 60000 && result < 3600000) {
			// 一小时内
			long seconds = result / 60000;
			r = seconds + "分钟前";
		} else if (result >= 3600000 && result < 86400000) {
			// 一天内
			long seconds = result / 3600000;
			r = seconds + "小时前";
		} else if (result >= 86400000 && result < 864000000) {
			// 10天内
			long seconds = result / 86400000;
			r = seconds + "天前";
		} else {
			// 日期格式
			r = format.format(date);
		}
		return r;
	}
	
	public static Timestamp parseTimestampString(String s){
		return Timestamp.valueOf(s);
	}
	
	public static boolean isToday(Timestamp t){
		if(t.getDay() == new Date().getDay()){
			return true;
		}
		return false;
	}
	
	public static void main(String[] args){
		
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		try {
//			System.out.println(df.format(df.parse("2013-1-28 22:57:04")));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		//2013-1-28 22:57:04
		//2013-01-28 22:57:04
		//System.out.println(DateUtil.getYear(new Date()));
		// System.out.println(DateUtil.parseTimestampString(DateUtil.getDate("2013-1-28 22:57:04", DateUtil.format3).toString()));
//		String s=StringUtils.join(StringUtils.split(DateUtil.getNowDate(),"-"));
//		System.out.println(s);
		//String a = "2010-01-15 10:56:34";
//		String b = "2010-04-16 11:59:36";
//		//7862400000
		//long am = DateUtil.getDiffInfoByDay("2013-1-24","2013-2-7");
		Date date=new Date();
		SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
		System.out.println(dateFm.format(date));
		
		System.out.println(DateUtil.getWeekOfDate(date));
//		//long sec = DateUtil.getSecDiff("2010-09-13 10:59:12", am);
//		String c = DateUtil.getNowDateTime();
//		String am = DateUtil.getMonthAfter(c, 3);
//		System.out.println(am);
//		long sec = DateUtil.getSecDiff(c, am);
//		System.out.println(sec + "");
//		System.out.println(DateUtil.chageToDHMS(sec*1000));
		//System.out.println(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		//System.out.println(DateUtil.parseTimestampString(a));
	}
}
