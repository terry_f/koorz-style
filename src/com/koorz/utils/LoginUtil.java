package com.koorz.utils;

import java.util.HashMap;
import java.util.Map;

import com.koorz.oauth.builder.ServiceBuilder;
import com.koorz.oauth.builder.api.DoubanApi;
import com.koorz.oauth.builder.api.RenrenApi;
import com.koorz.oauth.builder.api.SinaWeiboApi;
import com.koorz.oauth.builder.api.TaobaoApi20;
import com.koorz.oauth.builder.api.TencentApi;
import com.koorz.oauth.oauth.OAuthService;


/**
 * 功能说明：用户授权相关
 * 创建者：尹东东
 * 创建时间：2012-7-30 下午10:06:30
 * 版本号：1.0
 */
public class LoginUtil {
	private Map<Integer,OAuthService> authService = new HashMap<Integer,OAuthService>();
	
	private static class SingletonHolder { 
		static final LoginUtil INSTANCE = new LoginUtil(); 
	}
	
	public static LoginUtil getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	private LoginUtil() {
		// 初始化导航链接
		for(int i=0;i<WeiboType.values().length;i++){
			//0、新浪微博；1、腾讯微博；2、豆瓣；3、人人；4、QQ空间;5、淘宝
			if(i == 0){//新浪
				OAuthService service = new ServiceBuilder()
				.provider(SinaWeiboApi.class)
				.apiKey(SystemConfig.getValue("sina_client_ID"))
		        .apiSecret(SystemConfig.getValue("sina_client_SERCRET"))
		        .callback(SystemConfig.getValue("sina_redirect_URI"))
		        .state("0")
		        .build();
				authService.put(i, service);
			}else if(i == 1){//腾讯
				OAuthService service = new ServiceBuilder()
				.provider(TencentApi.class)
				.apiKey(SystemConfig.getValue("tercent_client_ID"))
		        .apiSecret(SystemConfig.getValue("tercent_client_SERCRET")).callback(SystemConfig.getValue("tercent_redirect_URI"))
		        .scope("get_user_info,add_share,add_t,get_fanslist")
		        .state("1")
		        .build();
				authService.put(i, service);
			}else if(i == 2){//豆瓣
				OAuthService service = new ServiceBuilder()
				.provider(DoubanApi.class)
				.apiKey(SystemConfig.getValue("douban_client_ID"))
		        .apiSecret(SystemConfig.getValue("douban_client_SERCRET")).callback(SystemConfig.getValue("douban_redirect_URI"))
		        .scope("douban_basic_common,shuo_basic_r,shuo_basic_w,music_basic_r,music_basic_w,music_artist_r")
		        .state("2")
		        .build();
				authService.put(i, service);
			}else if(i == 3){//人人
				OAuthService service = new ServiceBuilder()
				.provider(RenrenApi.class)
				.apiKey(SystemConfig.getValue("renren_client_ID"))
		        .scope("publish_share publish_feed").apiSecret(SystemConfig.getValue("renren_client_SERCRET")).callback(SystemConfig.getValue("renren_redirect_URI"))
		        .state("3")
		        .build();
				authService.put(i, service);
			}else if(i == 5){
				OAuthService service = new ServiceBuilder()
				.provider(TaobaoApi20.class)
				.apiKey(SystemConfig.getValue("taobao_client_ID"))
		        .scope("item").apiSecret(SystemConfig.getValue("taobao_client_SERCRET")).callback(SystemConfig.getValue("taobao_redirect_URI"))
		        .state("5")
		        .build();
				authService.put(i, service);
			}
		}
	}
	
	/**
	 * 
	 * 功能描述：type 授权类型，如新浪，淘宝等
	 * 作        者：尹东东
	 * 创建时间：2013-1-26 下午10:13:55
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public OAuthService getOAuthService(int type){
		if(!authService.isEmpty()){
			for(Integer key:authService.keySet()){
				if(key == type){
					 return authService.get(key);
				}
			}
		}
		return null;
	}
	
	/**
	 * 
	 * 功能描述：判断是否是支持的登录类型
	 * 作        者：尹东东
	 * 创建时间：2013-2-1 下午3:34:08
	 * 参       数：
	 * 返       回:true表示支持
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static boolean hasType(int type){
		boolean flag = false;
		if(type == WeiboType.SINA.ordinal()){
			flag = true;
		}else if(type == WeiboType.TERCENT.ordinal()){
			flag = true;
		}else if(type == WeiboType.DOUBAN.ordinal()){
			flag = true;
		}else if(type == WeiboType.RENREN.ordinal()){
			flag = true;
		}else if(type == WeiboType.QQSPACE.ordinal()){
			flag = true;
		}else if(type == WeiboType.TAOBAO.ordinal()){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 
	 * 功能描述：验证是否我们网站传给第三方的值
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 下午5:15:57
	 * 参       数:state
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static boolean validate(String state){
		return hasType(getState(state));
	}
	
	public static int getState(String state){
//		String ck = CryptUtils.decrypt(state);
//		final String[] items = StringUtils.split(ck, '|');
//		if (items.length == 3) {
//			return Integer.parseInt(items[1]);
//		}else {
//			return -1;
//		}
		return Integer.parseInt(state);
	}
	
	/**
	 * 
	 * 功能描述：生成传递给第三方平台的值
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 下午5:20:05
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String createState(String snsType){
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTime(new Date());
//		
//		StringBuilder sb = new StringBuilder();
//		sb.append(calendar.getTimeInMillis());
//		sb.append('|');
//		sb.append(snsType);
//		sb.append('|');
//		calendar.add(Calendar.MINUTE, 5);
//		sb.append(calendar.getTimeInMillis());
//		return CryptUtils.encrypt(sb.toString());
		//编码导致回传回来的和原来的不一样了 tDrLn8zHarHZ3LFQ%2BT2tzi%2F3i13Bn6poZ0Wp2kKTsnw%3D  tDrLn8zHarHZ3LFQ%20T2tzi%2F3i13Bn6poZ0Wp2kKTsnw%3D
		return snsType;
	}
	
	public enum WeiboType {
	     SINA,TERCENT,DOUBAN,RENREN,QQSPACE,TAOBAO
	}
	
	public static void main(String[] args) {
		//String state="BPcwntO8tVH0PL+imVJUAJzvomZWQpDzck7WU02zatQ=";
		String state="BPcwntO8tVH0PL%2BimVJUAJzvomZWQpDzck7WU02zatQ%3D";
		///user/snsBind?state=BPcwntO8tVH0PL%2BimVJUAJzvomZWQpDzck7WU02zatQ%3D&code=4c190c6c501645a96773bab17a852c02
		//String state = LoginUtil.createState("6");
		//System.out.println(state);
		System.out.println(CryptUtils.decrypt(state));
		if(validate(state)){
			System.out.println("通过");
		}
	}
}
