/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import com.alibaba.fastjson.JSONObject;

/**
 * 功能描述：请求action成功和失败提示
 * 作        者：尹东东 
 * 创建时间：2013-5-13 下午11:23:55
 * 版  本  号：1.0
 */
public class ActionUtils {
	//失败code
	public static final String CODE_100 = "100";
	//成功code
	public static final String CODE_200 = "200";
	//成功
	public static final String SUCCESS = "操作成功";
	//失败
	public static final String FAILED = "操作失败";
	
	public static String result(String code,String msg){
		JSONObject json = new JSONObject();
		json.put("code", code);
		json.put("msg", msg);
		return json.toJSONString();
	}
	
	public static String result(String code,String msg,String data){
		JSONObject json = new JSONObject();
		json.put("code", code);
		json.put("msg", msg);
		json.put("data", data);
		return json.toJSONString();
	}
}
