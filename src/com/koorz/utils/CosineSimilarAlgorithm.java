/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.koorz.jfinal.lucene.SearchHelper;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-30 下午4:40:55
 * 版  本  号：1.0
 */
public class CosineSimilarAlgorithm {
	
	public static double getSimilarity(String src, String target) {
		if (!StringUtils.isEmpty(src) && !StringUtils.isEmpty(target)) {
			Map<String, int[]> AlgorithmMap = new HashMap<String, int[]>();
			for(String word : SearchHelper.splitKeywords(src)){
				int[] fq = AlgorithmMap.get(word);
				if(fq != null && fq.length == 2){
					fq[0]++;
				}else {
					fq = new int[2];
					fq[0] = 1;
					fq[1] = 0;
					AlgorithmMap.put(word, fq);
				}
				System.out.println("src:"+word);
		    }
			for(String word : SearchHelper.splitKeywords(target)){
				int[] fq = AlgorithmMap.get(word);
				if(fq != null && fq.length == 2){
					fq[1]++;
				}else {
					fq = new int[2];
					fq[0] = 0;
					fq[1] = 1;
					AlgorithmMap.put(word, fq);
				}
				System.out.println("target:"+word);
		    }
			
			Iterator<Entry<String, int[]>> iterator = AlgorithmMap.entrySet().iterator();
			double sqdoc1 = 0;
			double sqdoc2 = 0;
			double denominator = 0; 
			while(iterator.hasNext()){
				int[] c = iterator.next().getValue();
				denominator += c[0]*c[1];
				sqdoc1 += c[0]*c[0];
				sqdoc2 += c[1]*c[1];
			}
			
			return denominator / Math.sqrt(sqdoc1*sqdoc2);
		} else {
			throw new NullPointerException(
					" the Document is null or have not cahrs!!");
		}
	}

	public static boolean isHanZi(char ch) {
		// 判断是否汉字
		return (ch >= 0x4E00 && ch <= 0x9FA5);

	}

	/**
	 * 根据输入的Unicode字符，获取它的GB2312编码或者ascii编码，
	 * 
	 * @param ch
	 *            输入的GB2312中文字符或者ASCII字符(128个)
	 * @return ch在GB2312中的位置，-1表示该字符不认识
	 */
	public static short getGB2312Id(char ch) {
		try {
			byte[] buffer = Character.toString(ch).getBytes("GB2312");
			if (buffer.length != 2) {
				// 正常情况下buffer应该是两个字节，否则说明ch不属于GB2312编码，故返回'?'，此时说明不认识该字符
				return -1;
			}
			int b0 = (int) (buffer[0] & 0x0FF) - 161; // 编码从A1开始，因此减去0xA1=161
			int b1 = (int) (buffer[1] & 0x0FF) - 161; // 第一个字符和最后一个字符没有汉字，因此每个区只收16*6-2=94个汉字
			return (short) (b0 * 94 + b1);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public static void main(String[] args) {
		//0.4879500364742666
		String doc1 = "街拍达人，黑色修身薄款西服外套+白色蕾丝包臀背心裙+黑色夹脚平底凉鞋";
		String doc2 = "街拍达人，紫色长款雪纺衬衫+灰色包臀背心裙+白色菱格帆布鞋+黑色串珠多层项链";
		System.out.println(getSimilarity(doc1,doc2));
	}
}
