/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * 功能描述：系统配置文件
 * 作        者：尹东东 
 * 创建时间：2013-1-14 下午8:32:50
 * 版  本  号：1.0
 */
public class SystemConfig {
	private static Properties props = new Properties(); 
	static{
		try {
			props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("system_cfg.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static String getValue(String key){
		return props.getProperty(key);
	}
	public static String getValue(String key,String def){
		return props.getProperty(key,def);
	}
	public static Boolean getToBool(String key,Boolean def){
		try{
			return Boolean.valueOf(props.getProperty(key));
		}catch(Exception e){
			return def;
		}
	}
	public static Integer getToInteger(String key,Integer def){
		try{
			return Integer.valueOf(props.getProperty(key));
		}catch(Exception e){
			return def;
		}
	}
	public static Long getToLong(String key,Long def){
		try{
			return Long.valueOf(props.getProperty(key));
		}catch(Exception e){
			return def;
		}
	}
	
	public static Double getToDouble(String key,Double def){
		try{
			return Double.valueOf(props.getProperty(key));
		}catch(Exception e){
			return def;
		}
	}
	
	/**
	 * 
	 * 功能描述：获取上传文件的最大值
	 * 作        者：尹东东
	 * 创建时间：2013-1-26 上午11:25:19
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static int getUploadMaxSize(){
		return getToInteger("file.max_size",2*1024*1024);
	}
	
	/**
	 * 
	 * 功能描述：获取上传文件的临时目录
	 * 作        者：尹东东
	 * 创建时间：2013-1-26 上午11:25:00
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String getTempDir(){
		if(createDir(getValue("temp_dir"))){
			return getValue("temp_dir");
		}else {
			return "";
		}
	}
	
	/**
	 * 
	 * 功能描述：获取索引目录
	 * 作        者：尹东东
	 * 创建时间：2013-3-19 上午10:07:03
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String getIndexDir(){
		if(createDir(getValue("lucene_index_dir"))){
			return getValue("lucene_index_dir");
		}else {
			return getValue("lucene_index_dir");
		}
	}
	
	private static boolean createDir(String dir){
		boolean flag = true;
		File file = new File(dir);
		if(!file.exists()){
			flag = file.mkdirs();
		}
		return flag;
	}
	
    public static void main(String[] args) {
    	System.out.println(getUploadMaxSize());
//    	String searchPath = SystemConfig.getValue("img.graphicsmagick_path","");
//    	if(!StringUtils.isEmpty(searchPath)){
//    		System.out.println("path:"+searchPath);
//    	}
//    	System.out.println(searchPath);
	}
}
