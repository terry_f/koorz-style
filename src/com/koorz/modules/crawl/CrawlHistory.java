/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-30 下午10:57:29
 * 版  本  号：1.0
 */
public class CrawlHistory extends Model<CrawlHistory>{
	private static final long serialVersionUID = -8188331259944879147L;
	public static final CrawlHistory dao = new CrawlHistory();
	
	public static void saveCrawlHistory(CrawlHistory c){
		c.set("id", IdManage.nextId(IdManage.CRAWL_HISTORY)).set("create_time", DateUtil.getNowDateTime()).save();
	}
}
