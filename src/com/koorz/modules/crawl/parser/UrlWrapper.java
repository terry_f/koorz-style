/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;
/**
 * 功能描述：url请求路径及相关参数封装
 * 作        者：尹东东 
 * 创建时间：2013-6-5 下午7:27:04
 * 版  本  号：1.0
 */
public class UrlWrapper {
	/**
	 * 请求地址
	 */
	private String url;
	
	/**
	 * 请求地址对应的分类名称
	 */
	private String category;
	
	/**
	 * 请求地址对应的菜单下的标签组名称
	 */
	private String labelGroup;
	
	/**
	 * 请求地址对应的菜单下的标签组下的标签名称
	 */
	private String label;
	
	/**
	 * 分类id
	 */
	private String categoryId;
	
	/**
	 * 标签组id
	 */
	private String labelGroupId;
	
	/**
	 * 标签id
	 */
	private String labelId;

	public String getUrl() {
		return url;
	}

	public String getCategory() {
		return category;
	}

	public String getLabelGroup() {
		return labelGroup;
	}

	public String getLabel() {
		return label;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public String getLabelGroupId() {
		return labelGroupId;
	}

	public String getLabelId() {
		return labelId;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setLabelGroup(String labelGroup) {
		this.labelGroup = labelGroup;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public void setLabelGroupId(String labelGroupId) {
		this.labelGroupId = labelGroupId;
	}

	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}

	public UrlWrapper() {}
	
	public UrlWrapper(String url, String category, String labelGroup, String label) {
		this.url = url;
		this.category = category;
		this.labelGroup = labelGroup;
		this.label = label;
	}

	public UrlWrapper(String url, String category, String labelGroup, String label,
			String categoryId, String labelGroupId, String labelId) {
		this.url = url;
		this.category = category;
		this.labelGroup = labelGroup;
		this.label = label;
		this.categoryId = categoryId;
		this.labelGroupId = labelGroupId;
		this.labelId = labelId;
	}
	
}
