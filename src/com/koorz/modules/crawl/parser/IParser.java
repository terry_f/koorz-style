/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;

import javax.servlet.http.Cookie;

/**
 * 功能描述：解析器
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午8:16:55
 * 版  本  号：1.0
 */
public interface IParser {
	
	public IParser setCookie(String cookie);
	
	public IParser setCookies(Cookie[] cookies);
	
	public String getCookie();
	
	public Cookie[] getCookies();
	
	/**
	 * 
	 * 功能描述：创建任务
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午10:00:34
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void createTask();
	
	/**
	 * 
	 * 功能描述：分配process处理任务
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午10:00:57
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void handler();
}
