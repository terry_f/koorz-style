/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.commission;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：提现记录
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:01:52
 * 版  本  号：1.0
 */
public class DrawRecord extends Model<DrawRecord>{
	private static final long serialVersionUID = 3306235449099511766L;
	public static final DrawRecord dao = new DrawRecord();
	
	public static void saveDrawRecord(DrawRecord d){
		d.set("id", IdManage.nextId(IdManage.DRAW_RECORD)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 功能描述：提现记录审核状态  提现状态：0:处理中、1提现成功、2提现失败
	 * 作        者：尹东东 
	 * 创建时间：2013-6-13 上午11:29:32
	 * 版  本  号：1.0
	 */
	public enum Status {
		NOT_CHECK,SUCCESS,FAILED
	}
}