/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.commission;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.koorz.modules.user.User;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：系统总发行虚拟币
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:03:48
 * 版  本  号：1.0
 */
public class VirtualCurrency extends Model<VirtualCurrency>{
	private static final long serialVersionUID = 412770249470879287L;
	public static final VirtualCurrency dao = new VirtualCurrency();
	
	public static void saveVirtualCurrency(VirtualCurrency v){
		v.set("id", IdManage.nextId(IdManage.VIRTUAL_CURRENCY)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：获得系统总佣金数
	 * 作        者：尹东东
	 * 创建时间：2013-6-13 下午3:26:27
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static double getTotalCommission(){
		VirtualCurrency totalCommission = dao.findFirst("select SUM(money) as total from virtual_currency");
		return totalCommission.getDouble("total")==null?0.0:totalCommission.getDouble("total");
	}
	
	/**
	 * 
	 * 功能描述：获取系统挣取得总佣金数
	 * 作        者：尹东东
	 * 创建时间：2013-6-13 下午3:27:54
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static double getSystemCommission(){
		//手续费所得
		Record charge = Db.findFirst("select SUM(money) as total from virtual_account where type=?",VirtualAccount.Type.CHARGE.ordinal());
		//分成所得
		Record percentage = Db.findFirst("select SUM(money) as total from virtual_account where type=?",VirtualAccount.Type.PERCENTAGE.ordinal());
		//奖励支出
		Record reward = Db.findFirst("select SUM(money) as total from virtual_account where type=?",VirtualAccount.Type.REWARD.ordinal());
		double income = (charge.getDouble("total") == null?0.0:charge.getDouble("total")) +
				(percentage.getDouble("total") == null?0.0:percentage.getDouble("total"));
		double expenditure = reward.getDouble("total") == null?0.0:reward.getDouble("total");
		return income - expenditure;
	}
	
	/**
	 * 
	 * 功能描述：获取所有用户拥有的佣金数
	 * 作        者：尹东东
	 * 创建时间：2013-6-13 下午3:28:36
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static double getUserCommission(){
		Record userRecord = Db.findFirst("select SUM(account_balance) as total from user where type=?",User.UserType.SELF.ordinal());
		double user = userRecord.getDouble("total")==null?0.0:userRecord.getDouble("total");
		return getDrawCommission() + user;
	}
	
	/**
	 * 
	 * 功能描述：获取所有用户已提现部分
	 * 作        者：尹东东
	 * 创建时间：2013-6-13 下午4:03:11
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static double getDrawCommission(){
		Record drawRecord = Db.findFirst("select SUM(money) as total from virtual_account where type=?",VirtualAccount.Type.DRAW.ordinal());
		return drawRecord.getDouble("total") == null?0.0:drawRecord.getDouble("total");
	}
}
