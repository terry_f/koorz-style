/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.kpi;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：奖励指标
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:03:08
 * 版  本  号：1.0
 */
public class RewardStandards extends Model<RewardStandards>{
	private static final long serialVersionUID = 5589857515275143237L;
	public static final RewardStandards dao = new RewardStandards();
	
	public static void saveRewardStandards(RewardStandards r){
		r.set("id", IdManage.nextId(IdManage.REWARD_STANDARDS)).save();
	}
	
	/**
	 * 
	 * 功能描述：获取所有指标
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 上午11:05:13
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<RewardStandards> getRewardStandardsList(){
		return dao.find("select * from reward_standards");
	}
}
