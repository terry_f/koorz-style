/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.kpi;

import org.apache.commons.lang.StringUtils;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：奖励记录
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:02:23
 * 版  本  号：1.0
 */
public class RewardRecord extends Model<RewardRecord>{
	private static final long serialVersionUID = -8880033576054823972L;
	public static final RewardRecord dao = new RewardRecord();
	
	public static void saveRewardRecord(RewardRecord r){
		r.set("id", IdManage.nextId(IdManage.REWARD_RECORD)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：获取用户完成奖励指标情况,每个月的21号统计的，所有只取上个月21后 到这个月21号的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 下午3:57:59
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Record> getUserCompleteReward(int page,int pageSize){
		String select = "select sum(of.commission_rate*of.real_pay_fee) as total,u.nickname,of.user_id";
		String sqlExceptSelect = "from order_form as of " +
				" inner join user as u on u.id=of.user_id " +
				" where DATE_FORMAT(of.pay_time,'%Y-%m-%d') > ? and DATE_FORMAT(of.pay_time,'%Y-%m-%d') <= CURDATE() " +
				" group by of.user_id " +
				" order by pay_time desc";
		String preTime = DateUtil.getMonthAdd(DateUtil.getDate(DateUtil.getNowDate()), -1);
		int day = Integer.parseInt(StringUtils.substringAfterLast(preTime,"-"));
		preTime = DateUtil.getDayAdd(DateUtil.getDate(preTime), 21-day);
		return Db.paginate(page, pageSize, select, sqlExceptSelect,preTime);
	}
}
