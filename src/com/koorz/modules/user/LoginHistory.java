/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：登录历史
 * 作        者：尹东东 
 * 创建时间：2013-1-6 下午2:38:58
 * 版  本  号：1.0
 */
public class LoginHistory extends Model<LoginHistory>{
	private static final long serialVersionUID = -4123112636002805080L;
	public static final LoginHistory dao = new LoginHistory();
}
