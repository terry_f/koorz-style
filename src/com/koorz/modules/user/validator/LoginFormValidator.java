/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user.validator;


import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;
import com.koorz.modules.user.User;

/**
 * 功能描述： 作 者：尹东东 创建时间：2013-1-6 下午2:03:07 版 本 号：1.0
 */
public class LoginFormValidator extends Validator {
	
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequiredString("username", "user_error", "请输入用户名");
		validateRequiredString("password", "password_error", "请输入密码");
		if(!User.validate(c.getPara("username"), c.getPara("password"))){
			addError("user_role_error", "用户不存在或密码错误");
		}
	}

	protected void handleError(Controller c) {
		c.keepPara("username","password");
		c.renderJson();
	}
}
