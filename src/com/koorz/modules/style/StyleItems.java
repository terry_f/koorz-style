/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.style;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：搭配项
 * 作        者：尹东东 
 * 创建时间：2013-5-23 下午9:10:54
 * 版  本  号：1.0
 */
public class StyleItems extends Model<StyleItems> {
	private static final long serialVersionUID = -1072522360523156352L;
	public static final StyleItems dao = new StyleItems();
	
	public static void saveStyleItems(StyleItems s){
		s.set("id", IdManage.nextId(IdManage.STYLE_ITEMS)).save();
	}
	
	/**
	 * 
	 * 功能描述：根据搭配id获取搭配项
	 * 作        者：尹东东
	 * 创建时间：2013-5-28 下午7:43:33
	 * 参       数:搭配id
	 * 返       回:搭配项列表
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<StyleItems> getStyleItems(String id){
		return dao.find("select si.style_id,si.sequence,si.image_id,i.style_url,i.style_width,i.style_height from style_items as si " +
				" inner join image as i on i.id=si.image_id " +
				" where si.style_id=? " +
				" order by si.sequence asc ", id);
	}
}
