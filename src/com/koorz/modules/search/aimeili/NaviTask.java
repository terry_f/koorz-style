/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search.aimeili;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.log.Logger;
import com.koorz.modules.crawl.CrawlHistory;
import com.koorz.modules.image.Image;
import com.koorz.modules.search.SimpleHttpClient;
import com.koorz.modules.search.TaskService;
import com.koorz.modules.search.ThreadPoolMananger;
import com.koorz.modules.search.UrlWrapper;
import com.koorz.utils.DateUtil;

/**
 * 功能描述：获取导航页数据任务
 * 作        者：尹东东 
 * 创建时间：2013-5-27 下午3:58:21
 * 版  本  号：1.0
 */
public class NaviTask implements Runnable{
	protected final Logger logger = Logger.getLogger(getClass());
	
	@Override
	public void run() {
		SimpleHttpClient client = new SimpleHttpClient();
		TaskService taskService = TaskService.getInstance();
		AimeiliParser parser = AimeiliParser.getInstance();
		while (true) {
			if(parser.isStopCrawl()){
				logger.debug("NaviTask 导航链接抓取完成");
				break;
			}
			UrlWrapper navi = parser.getNavi();
			if(navi != null){
				String result = client.get(navi.getUrl(),parser.getCookie());
				if(StringUtils.isEmpty(result)){
					continue;
				}
				JSONArray jsonArray = null;
				try {
					JSONObject json = JSON.parseObject(result);
					JSONObject data = json.getJSONObject("data");
					jsonArray = data.getJSONArray("templates");
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("NaviTask 获取导航数据失败！"+navi.getUrl(), e);
					continue;
				}
				if(jsonArray != null){
					for(int i=0;i<jsonArray.size();i++){
						JSONObject content = jsonArray.getJSONObject(i);
						CrawlHistory crawl = CrawlHistory.dao.findFirst("select * from crawl_history where url=?", content.getString("bg_image"));
						if(crawl != null){
							continue;
						}
						
						//保存主图
						Image image = new Image();
						image.set("description", content.getString("template_name"));
						image.set("url", content.getString("bg_image"));
						image.set("check_state", Image.ImageState.SHOW.ordinal());
						image.set("check_time", DateUtil.getNowDateTime());
						image.set("type", Image.ImageType.THEME.ordinal());
						image.set("source_id", content.getString("_id"));
						image.set("user_id", "100000");
						image = taskService.saveImage(image);
						
						taskService.addImageToLabel(image.getStr("id"), navi.getLabel());
						taskService.addImageToCategory(image.getStr("id"), navi.getCategoryId());
						taskService.addImageToLabelGroup(image.getStr("id"), navi.getLabelGroupId());
						
						///下载主图图片
						try {
							parser.pushQueue();
							ThreadPoolMananger.ImageQueue.put(image);
						} catch (InterruptedException e) {
							e.printStackTrace();
							logger.error("NaviTask 添加图片队列--image 失败！", e);
						}
					}
				}else {
					continue;
				}
				parser.setCrawlCount();
			}
		}
		client.close();
	}

}
