/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-27 下午2:14:03
 * 版  本  号：1.0
 */
public class ThreadPoolMananger {
	//下载图片队列
	public static final BlockingQueue<Model> ImageQueue = new LinkedBlockingQueue<Model>(60);

	final ExecutorService exec = Executors.newFixedThreadPool(60);
	
	public void addTask(Runnable task){
		exec.execute(task);
	}
}
