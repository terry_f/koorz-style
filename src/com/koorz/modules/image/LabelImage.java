/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.image;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：图片所属标签实体
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午3:41:17
 * 版  本  号：1.0
 */
public class LabelImage extends Model<LabelImage>{
	private static final long serialVersionUID = -933528619694005425L;
	public static final LabelImage dao = new LabelImage();
}
