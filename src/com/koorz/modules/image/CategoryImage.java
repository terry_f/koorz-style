/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.image;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：图片所属分类实体
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午3:40:53
 * 版  本  号：1.0
 */
public class CategoryImage extends Model<CategoryImage>{
	private static final long serialVersionUID = 5937098592964929586L;
	public static final CategoryImage dao = new CategoryImage();
}
