/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * 功能描述：记录用户最后一次请求的时间
 * 作        者：尹东东 
 * 创建时间：2013-5-21 下午5:12:15
 * 版  本  号：1.0
 */
public class OnlineUserInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		ai.invoke();
//		Controller c = ai.getController();
//		c.getco
//		HttpSession hs = c.getSession(createSession);
//		if (hs != null) {
//			Map session = new JFinalSession(hs);
//			for (Enumeration<String> names=hs.getAttributeNames(); names.hasMoreElements();) {
//				String name = names.nextElement();
//				session.put(name, hs.getAttribute(name));
//			}
//			c.setAttr("session", session);
//		}
	}
}
