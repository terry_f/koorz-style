/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.checkStyle;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 功能描述：任务服务类
 * 作        者：尹东东 
 * 创建时间：2013-5-24 下午6:22:26
 * 版  本  号：1.0
 */
public class TaskService {
	
	private static TaskService singleton;
	private  Hashtable<String,ITask> tasks = new Hashtable<String,ITask>();
    
	private TaskService(){}
	
	public static TaskService getInstance(){
		if(singleton == null){
			synchronized(TaskService.class){
				if(singleton == null){
					singleton = new TaskService();
				}
			}
		}
		return singleton;
	}
	
	/**
	 * 
	 * 功能描述：添加任务
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午6:38:18
	 * 参       数:任务id；启动的task
	 * 返       回:true表示添加成功；false表示已经存在任务
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public boolean addTask(String id,ITask task){
		ITask t = getTask(id);
		if(t == null || (t != null && !t.getId().equals(task.getId()))){
			tasks.put(id, task);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * 功能描述：获取任务
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午6:40:35
	 * 参       数:任务id
	 * 返       回:返回id对应的任务
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public ITask getTask(String id){
		if(!tasks.isEmpty()){
			return tasks.get(id);
		}
		return null;
	}
	
	/**
	 * 
	 * 功能描述：停止任务
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午7:49:30
	 * 参       数:任务id；停止的task
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void stopTask(ITask task,String id){
		task.stop();
		tasks.remove(id);
		task = null;
	}
	
	/**
	 * 
	 * 功能描述：获取所有任务中的数据
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午8:38:08
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public List<String> getAllData(){
		List<String> result = new ArrayList<String>();
		if(!tasks.isEmpty()){
			Set<Entry<String,ITask>> entrys = tasks.entrySet();
			for(Entry<String,ITask> entry:entrys){
				ITask task = entry.getValue();
				if(task.getData() != null)
				result.addAll(task.getData());
			}
		}
		return result;
	}
}
