/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.resource;

import com.jfinal.plugin.IPlugin;

/**
 * 功能描述：系统启动时初始化静态资源
 * 作        者：尹东东 
 * 创建时间：2013-4-22 下午8:20:33
 * 版  本  号：1.0
 */
public class ResourcePlugin implements IPlugin{

	private String configFileName;
	
	public ResourcePlugin(String configFileName) {
		this.configFileName = configFileName;
	}

	@Override
	public boolean start() {
		try {
			ResourceKit.init(configFileName);
		} catch (Exception e) {
			new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean stop() {
		ResourceKit.clearMap();
		return true;
	}
}
