/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.resource;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-22 下午8:24:28
 * 版  本  号：1.0
 */
@XmlRootElement
class Item {
	@XmlAttribute 
	String order;

	@XmlValue
	String value;
}
