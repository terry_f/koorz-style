/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.resource;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-22 下午8:23:35
 * 版  本  号：1.0
 */
@XmlRootElement
class Groups {
	@XmlAttribute 
	String name;
	
	@XmlAttribute 
	String suffix;
	
	@XmlAttribute 
	String host;
	
    @XmlElement(name="group")
	List<Group> groups = new ArrayList<Group>();
}
