/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.resource;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.jfinal.log.Logger;
import com.koorz.utils.DateUtil;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-22 下午8:41:52
 * 版  本  号：1.0
 */
public class ResourceKit {

	protected final static Logger logger = Logger.getLogger(ResourceKit.class);
	private static Map<String, List<String>> jsMap;
	private static Map<String, List<String>> cssMap;

	/**
	 * 
	 * 功能描述：获取js文件
	 * 作        者：尹东东
	 * 创建时间：2013-4-22 下午10:02:40
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<String> getJs(String view){
		if(jsMap ==null){
			throw new NullPointerException("ResourcePlugin not start");
		}
		List<String> result = new ArrayList<String>();
		
		result.addAll(jsMap.get("commons"));
		List<String> jss = jsMap.get(view);
		if(jss != null && jss.size() > 0) result.addAll(jss);
		return result;
	}
	
	/**
	 * 
	 * 功能描述：获取css文件
	 * 作        者：尹东东
	 * 创建时间：2013-4-22 下午10:35:01
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<String> getCss(String view){
		if(cssMap ==null){
			throw new NullPointerException("ResourcePlugin not start");
		}
		List<String> result = new ArrayList<String>();
		List<String> csss = cssMap.get(view);
		if(csss != null && csss.size() > 0) result.addAll(csss);
		return result;
	}

	static void clearMap() {
		jsMap.clear();
		cssMap.clear();
	}
	
	static void init(String configFileName) {
		jsMap =  new HashMap<String, List<String>>();
		cssMap =  new HashMap<String, List<String>>();
		File xmlfile = new File(ResourceKit.class.getClassLoader().getResource(configFileName).getFile());
		Resource resource = null;
		try {
			JAXBContext context = JAXBContext.newInstance(Resource.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			resource = (Resource) unmarshaller.unmarshal(xmlfile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		for (Groups groups : resource.groups) {
			String suffix = String.format(groups.suffix, DateUtil.getNowDate());
			for(Group group:groups.groups){
				if(group.items.size() > 0){
					List<String> path = new ArrayList<String>();
					for(Item item:group.items){
						path.add(groups.host+item.value+suffix);
					}
					if("js".equals(groups.name)){
						jsMap.put(group.name, path);
					}else {
						cssMap.put(group.name, path);
					}
				}
			}
		}
	}
}
