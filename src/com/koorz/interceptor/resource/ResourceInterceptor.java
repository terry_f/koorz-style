/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.resource;

import javax.servlet.http.Cookie;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.koorz.utils.taobaoke.TaobaokeConfig;

/**
 * 功能描述：静态资源上下文拦截器
 * 作        者：尹东东 
 * 创建时间：2013-1-11 下午5:28:30
 * 版  本  号：1.0
 */
public class ResourceInterceptor implements Interceptor {
	private String jsPathName;
	private String cssPathName;
	
	public ResourceInterceptor() {
		jsPathName = "RESOURCE_JS_PATH";
		cssPathName = "RESOURCE_CSS_PATH";
	}

	@Override
	public void intercept(ActionInvocation ai) {
		ai.invoke();
		Controller controller = ai.getController();
		String key = ai.getActionKey();
		controller.setAttr(cssPathName, ResourceKit.getCss(key));
		controller.setAttr(jsPathName, ResourceKit.getJs(key));
		//淘宝客使用
		controller.setCookie(new Cookie("timestamp", TaobaokeConfig.timestamp));
		controller.setCookie(new Cookie("sign", TaobaokeConfig.getSign()));
	}
}
